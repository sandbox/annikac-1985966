<?php
/**
 * @file
 * Default theme implementation for PCP block
 *
 * Available variables:
 *  $user: Current user object.
 *  $percentages: Array of each profile's percentage complete.
 *
 * @see template_preprocess_pcp_profile_percent_complete()
 */
?>

<div class="pcp-wrapper pcp-all-status">
  <?php print theme('item_list', array (
    'items' => $variables['item_list'],
    'attributes' => array ('class' => array('profile-progress')),
  )); ?>
</div>
