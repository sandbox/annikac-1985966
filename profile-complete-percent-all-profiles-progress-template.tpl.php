<?php
/**
 * @file
 * Default theme implementation for PCP block
 *
 * Available variables:
 *  $user: Current user object.
 *  $current_percent: From 0 to 100% of how much the profile is complete.
 *  $percentages: Array of each profile's percentage complete.
 *
 * @see template_preprocess_pcp_profile_percent_complete()
 */
?>
<style type="text/css">
  .pcp-percent-bar.pcp-all-status { width: <?php print $current_percent; ?>%; }
</style>

<div class="pcp-wrapper pcp-all-status">
  <?php print t('!complete% Complete', array('!complete' => $current_percent)); ?>

  <div class="pcp-percent-bar-wrapper">
    <div class="pcp-percent-bar pcp-all-status"></div>
  </div>
</div>
